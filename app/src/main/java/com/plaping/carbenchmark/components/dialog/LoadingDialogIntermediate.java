package com.plaping.carbenchmark.components.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import android.view.WindowManager;


import com.plaping.carbenchmark.R;

import static android.graphics.Color.TRANSPARENT;
import static android.view.WindowManager.LayoutParams.FLAG_DIM_BEHIND;

/**
 * Created by saintzst on 6/1/2018 AD
 */
public class LoadingDialogIntermediate extends DialogFragment {

    public static final String TAG = LoadingDialogIntermediate.class.getSimpleName();
    private Context context;

    public static LoadingDialogIntermediate newInstance() {
        return new LoadingDialogIntermediate();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading_intermediate);
        setCancelable(false);
        setLayoutParams(dialog);
        return dialog;
    }

    private void setLayoutParams(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
            wlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setBackgroundDrawable(new ColorDrawable(TRANSPARENT));
            window.clearFlags(FLAG_DIM_BEHIND);
            window.setAttributes(wlp);
        }
    }
}
