package com.plaping.carbenchmark.components.ui.actionbar;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.base.BaseCustomViewGroup;
import com.plaping.carbenchmark.utilties.AppSession;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class MyActionBar extends BaseCustomViewGroup {

    @BindView(R.id.tv_evaluator_code)
    TextView tvEvaluatorCode;
    @BindView(R.id.tv_my_action_bar_header)
    TextView tvHeader;
    @BindView(R.id.imv_user_avatar)
    ImageView imvAvatar;

    private MyActionBarListener listener;

    public MyActionBar(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public MyActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public MyActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public MyActionBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.layout_my_action_bar, this);
    }

    private void initInstances() {
        ButterKnife.bind(this);
        tvEvaluatorCode.setText(AppSession.getInstance().getEvaluatorCode().toUpperCase());
        setTint(AppSession.getInstance().getUserColor());
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MyActionBar,
                defStyleAttr, defStyleRes);
        try {
            tvHeader.setText(a.getText(R.styleable.MyActionBar_text));
        } finally {
            a.recycle();
        }
    }

    public MyActionBar setText(String text) {
        tvHeader.setText(text);
        return this;
    }

    public MyActionBar setListener(MyActionBarListener listener) {
        this.listener = listener;
        return this;
    }

    public MyActionBar setTint(String hex) {
        imvAvatar.setColorFilter(Color.parseColor(hex));
        return this;
    }

    @OnClick(R.id.btn_logout)
    void onLogoutClicked() {
        if (listener != null) {
            listener.onLogout();
        }
    }

}
