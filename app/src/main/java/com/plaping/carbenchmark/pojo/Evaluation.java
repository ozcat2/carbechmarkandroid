package com.plaping.carbenchmark.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class Evaluation {


    @SerializedName("id")
    private int id;
    @SerializedName("level")
    private String level;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("num_criteria")
    private int numCriteria;
    @SerializedName("test_total")
    private int testTotal;
    @SerializedName("test_complete")
    private int testComplete;
    @SerializedName("status")
    private double status;
    @SerializedName("status_text")
    private String statusText;
    @SerializedName("created")
    private String created;
    @SerializedName("created_text")
    private String createdText;
    @SerializedName("updated")
    private String updated;
    @SerializedName("updated_text")
    private String updatedText;
    @SerializedName("criteria")
    private List<Criteria> criteria = null;
    private float completion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumCriteria() {
        return numCriteria;
    }

    public void setNumCriteria(int numCriteria) {
        this.numCriteria = numCriteria;
    }

    public List<Criteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<Criteria> criteria) {
        this.criteria = criteria;
    }

    public double getStatus() {
        return status;
    }

    public void setStatus(double status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }


    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public int getTestTotal() {
        return testTotal;
    }

    public void setTestTotal(int testTotal) {
        this.testTotal = testTotal;
    }

    public int getTestComplete() {
        return testComplete;
    }

    public void setTestComplete(int testComplete) {
        this.testComplete = testComplete;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getCreatedText() {
        return createdText;
    }

    public void setCreatedText(String createdText) {
        this.createdText = createdText;
    }

    public String getUpdatedText() {
        return updatedText;
    }

    public void setUpdatedText(String updatedText) {
        this.updatedText = updatedText;
    }

    public float getCompletion() {
        return completion;
    }

    public void setCompletion(float completion) {
        this.completion = completion;
    }
}
