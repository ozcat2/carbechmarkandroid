package com.plaping.carbenchmark.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class Event {

    @SerializedName("data")
    private List<Data> data = null;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
