package com.plaping.carbenchmark.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class Data {

    @SerializedName("id")
    private int id;
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("num_evaluation")
    private int numEvaluation;
    @SerializedName("num_car")
    private int numCar;
    @SerializedName("num_evaluator")
    private int numEvaluator;
    @SerializedName("evaluator")
    private Evaluator evaluator;
    @SerializedName("evaluations")
    private List<Evaluation> evaluations = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumEvaluation() {
        return numEvaluation;
    }

    public void setNumEvaluation(int numEvaluation) {
        this.numEvaluation = numEvaluation;
    }

    public int getNumCar() {
        return numCar;
    }

    public void setNumCar(int numCar) {
        this.numCar = numCar;
    }

    public int getNumEvaluator() {
        return numEvaluator;
    }

    public void setNumEvaluator(int numEvaluator) {
        this.numEvaluator = numEvaluator;
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public List<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    @Override
    public String toString() {
        return code == null ? "" : code;
    }
}
