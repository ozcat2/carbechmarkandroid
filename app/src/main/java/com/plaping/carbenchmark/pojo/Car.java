package com.plaping.carbenchmark.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class Car {

    @SerializedName("id")
    private int id;
    @SerializedName("brand")
    private String brand;
    @SerializedName("model")
    private String model;
    @SerializedName("icon_color")
    private String iconColor;
    @SerializedName("logo")
    private String logo;
    @SerializedName("logo_url")
    private String logoUrl;
    @SerializedName("created")
    private String created;
    @SerializedName("created_str")
    private String createdStr;
    @SerializedName("updated")
    private String updated;
    @SerializedName("updated_str")
    private String updatedStr;
    @SerializedName("benchmark")
    private Benchmark benchmark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedStr() {
        return createdStr;
    }

    public void setCreatedStr(String createdStr) {
        this.createdStr = createdStr;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdatedStr() {
        return updatedStr;
    }

    public void setUpdatedStr(String updatedStr) {
        this.updatedStr = updatedStr;
    }

    public Benchmark getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(Benchmark benchmark) {
        this.benchmark = benchmark;
    }
}
