package com.plaping.carbenchmark.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class Score {

    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("slide_type")
    private String slideType;
    @SerializedName("name_1")
    private String name1;
    @SerializedName("value_1")
    private float value1;
    @SerializedName("name_2")
    private String name2;
    @SerializedName("value_2")
    private float value2;
    @SerializedName("name_3")
    private String name3;
    @SerializedName("value_3")
    private float value3;
    @SerializedName("name_4")
    private String name4;
    @SerializedName("value_4")
    private float value4;
    @SerializedName("name_5")
    private String name5;
    @SerializedName("value_5")
    private float value5;
    @SerializedName("is_na_enable")
    private boolean isNaEnable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlideType() {
        return slideType;
    }

    public void setSlideType(String slideType) {
        this.slideType = slideType;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public float getValue1() {
        return value1;
    }

    public void setValue1(float value1) {
        this.value1 = value1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public float getValue2() {
        return value2;
    }

    public void setValue2(float value2) {
        this.value2 = value2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public float getValue3() {
        return value3;
    }

    public void setValue3(float value3) {
        this.value3 = value3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public float getValue4() {
        return value4;
    }

    public void setValue4(float value4) {
        this.value4 = value4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public float getValue5() {
        return value5;
    }

    public void setValue5(float value5) {
        this.value5 = value5;
    }

    public boolean isNaEnable() {
        return isNaEnable;
    }

    public void setNaEnable(boolean naEnable) {
        isNaEnable = naEnable;
    }
}
