package com.plaping.carbenchmark.utilties;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.plaping.carbenchmark.BuildConfig;
import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.base.Contextor;
import com.plaping.carbenchmark.pojo.Data;
import com.plaping.carbenchmark.pojo.Evaluation;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class AppSession {

    private static AppSession instance;

    private Context mContext;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public static AppSession getInstance() {
        if (instance == null)
            instance = new AppSession();
        return instance;
    }

    private AppSession() {
        mContext = Contextor.getInstance().getContext();
        mContext = Contextor.getInstance().getContext();
        String PREF_NAME = BuildConfig.APPLICATION_ID;
        pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.commit();
    }

    private static final String LOGGED_IN = "logged_in";
    private static final String EVENT_ID = "event_id";
    private static final String EVALUATOR_CODE = "evaluator_code";
    private static final String DATA = "data";
    private static final String CAR_ID = "car_id";
    private static final String API_ENDPOINT = "api_endpoint";
    private static final String USER_COLOR = "user_color";

    public void setEventId(int id) {
        editor.putInt(EVENT_ID, id);
        editor.apply();
    }

    public void setLoggedIn() {
        editor.putBoolean(LOGGED_IN, true);
        editor.apply();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(LOGGED_IN, false);
    }

    public int getEventId() {
        return pref.getInt(EVENT_ID, -1);
    }

    public void setEvaluatorCode(String code) {
        editor.putString(EVALUATOR_CODE, code);
        editor.apply();
    }

    public String getEvaluatorCode() {
        return pref.getString(EVALUATOR_CODE, "");
    }

    public void setData(Data mData) {
        String sData = new Gson().toJson(mData);
        editor.putString(DATA, sData);
        editor.commit();
    }

    public Data getData() {
        String sData = pref.getString(DATA, "");
        return sData.equals("") ? null : new Gson().fromJson(sData, Data.class);
    }

    public Evaluation getEvaluationById(int id) {
        Data data = getData();
        if (data != null && data.getEvaluations() != null) {
            for (Evaluation evaluation : data.getEvaluations()) {
                if (evaluation.getId() == id) {
                    return evaluation;
                }
            }
        }
        return null;
    }

    public void saveEvaluation(Evaluation mEvaluation) {
        Data data = getData();
        if (data != null && data.getEvaluations() != null) {
            for (int i = 0; i < data.getEvaluations().size(); i++) {
                Evaluation evaluation = data.getEvaluations().get(i);
                if (evaluation.getId() == mEvaluation.getId()) {
                    data.getEvaluations().set(i, mEvaluation);
                    setData(data);
                    break;
                }
            }
        }
    }

    public void setCarId(int id) {
        editor.putInt(CAR_ID, id);
        editor.apply();
    }

    public int getCarId() {
        return pref.getInt(CAR_ID, 0);
    }

    public void setApiEndpoint(String endpoint) {
        editor.putString(API_ENDPOINT, endpoint);
        editor.commit();
    }

    public String getApiEndpoint() {
        return pref.getString(API_ENDPOINT, mContext.getString(R.string.base_url));
    }

    public void setUserColor(String color) {
        editor.putString(USER_COLOR, color);
        editor.apply();
    }

    public String getUserColor() {
        return pref.getString(USER_COLOR, "#606060");
    }

    public void logout() {
        String endpoint = getApiEndpoint();
        editor.clear();
        editor.putString(API_ENDPOINT, endpoint);
        editor.apply();
    }
}
