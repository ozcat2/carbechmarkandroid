package com.plaping.carbenchmark.utilties;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.plaping.carbenchmark.base.Contextor;

/**
 * Created by piggipo on 11/16/2014.
 */
public class NetworkUtils {

    private ConnectivityManager connectivity;

    private static NetworkUtils instance;

    public static NetworkUtils getInstance() {
        if (instance == null)
            instance = new NetworkUtils();
        return instance;
    }

    private Context mContext;

    private NetworkUtils() {
        mContext = Contextor.getInstance().getContext();
        connectivity = (ConnectivityManager) Contextor.getInstance().getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public boolean isConnected() {
        NetworkInfo activeNetworkInfo = connectivity.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
