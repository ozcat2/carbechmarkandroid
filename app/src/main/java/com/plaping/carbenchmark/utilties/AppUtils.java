package com.plaping.carbenchmark.utilties;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

import com.plaping.carbenchmark.base.Contextor;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class AppUtils {

    private static AppUtils instance;

    public static AppUtils getInstance() {
        if (instance == null)
            instance = new AppUtils();
        return instance;
    }

    private Context mContext;

    private AppUtils() {
        mContext = Contextor.getInstance().getContext();
    }

    public int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public int pxToDp(Context context, int px) {
        return px / (context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public float getConvertedValue(int intVal) {
        float floatVal;
        floatVal = .5f * intVal;
        return floatVal;
    }

    public void getScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
        }
    }
}
