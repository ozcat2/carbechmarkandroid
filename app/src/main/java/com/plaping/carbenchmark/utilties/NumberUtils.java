package com.plaping.carbenchmark.utilties;

/**
 * Created by tachakornueng on 19/9/2018 AD
 */
public class NumberUtils {

    public static float round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (float) Math.round(value * scale) / scale;
    }
}
