package com.plaping.carbenchmark.utilties;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by tachakornueng on 11/8/2018 AD
 */
public class L {

    private static final String TAG = "carBenchmarkLog";

    public static void d(String message) {
        if (message != null) {
            Log.d(TAG, message);
        }
    }

    public static void w(String message) {
        Log.w(TAG, message);
    }

    public static void i(String message) {
        Log.i(TAG, message);
    }

    public static void e(String message) {
        Log.e(TAG, message);
    }

    public static void json(Object o) {
        if (o != null) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            d(gson.toJson(o));
        }
    }

    public static void longString(String message) {
        int maxLogSize = 3000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            Log.v(TAG, message.substring(start, end));
        }
    }
}
