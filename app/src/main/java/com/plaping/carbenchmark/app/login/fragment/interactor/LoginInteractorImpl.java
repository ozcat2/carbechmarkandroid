package com.plaping.carbenchmark.app.login.fragment.interactor;

import com.plaping.carbenchmark.manager.http.api.ApiManager;
import com.plaping.carbenchmark.manager.http.callback.CallbackListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void getEvent(CallbackListener listener) {
        ApiManager.getInstance().getRequest(listener);
    }

    @Override
    public void getEventById(int eventId, String evaluatorCode, CallbackListener listener) {
        Map<String, String> map = new HashMap<>();
        map.put("event_id", String.valueOf(eventId));
        map.put("evaluator_code", evaluatorCode);
        ApiManager.getInstance().getRequest(map, listener);
    }

}
