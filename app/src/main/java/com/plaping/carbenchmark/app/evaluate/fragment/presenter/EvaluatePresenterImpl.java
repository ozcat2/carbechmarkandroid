package com.plaping.carbenchmark.app.evaluate.fragment.presenter;

import com.plaping.carbenchmark.app.evaluate.fragment.interactor.EvaluateInteractor;
import com.plaping.carbenchmark.app.evaluate.fragment.interactor.EvaluateInteractorImpl;
import com.plaping.carbenchmark.app.evaluate.fragment.view.EvaluateView;
import com.plaping.carbenchmark.manager.http.callback.CallbackListenerAdapter;
import com.plaping.carbenchmark.pojo.Criteria;
import com.plaping.carbenchmark.pojo.Evaluation;
import com.plaping.carbenchmark.utilties.AppSession;

import static com.plaping.carbenchmark.app.evaluate.fragment.view.EvaluateFragment.FLAG_LOGOUT;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public class EvaluatePresenterImpl implements EvaluatePresenter {

    private EvaluateView evaluateView;
    private EvaluateInteractor interactor;
    private Evaluation evaluation;

    public EvaluatePresenterImpl(EvaluateView evaluateView, int evaluationId) {
        this.evaluateView = evaluateView;
        interactor = new EvaluateInteractorImpl();
        evaluation = AppSession.getInstance().getEvaluationById(evaluationId);
    }

    @Override
    public void getCriteriaList() {
        if (evaluation.getCriteria() != null && evaluation.getCriteria().size() > 0) {
            evaluation.getCriteria().get(0).setSelecting(true);
            evaluateView.setCriteriaList(evaluation.getCriteria());
        }
    }

    @Override
    public void saveEvaluation(Criteria mCriteria) {
        int criteriaId = mCriteria.getId();
        for (int i = 0; i < evaluation.getCriteria().size(); i++) {
            Criteria criteria = evaluation.getCriteria().get(i);
            if (criteria.getId() == criteriaId) {
                evaluation.getCriteria().set(i, mCriteria);
                AppSession.getInstance().saveEvaluation(evaluation);
                return;
            }
        }
        evaluateView.showMessage("Error, Unable to find the criteria in this evaluation");
    }

    @Override
    public void syncData(int flag) {
        interactor.syncData(AppSession.getInstance().getData(), new CallbackListenerAdapter() {
            @Override
            public void onSuccess(String response) {
                if (flag == FLAG_LOGOUT) {
                    evaluateView.navigateToLogin();
                } else {
                    evaluateView.showMessage("Success");
                }
            }

            @Override
            public void onError(String error) {
                super.onError(error);
                if (flag == FLAG_LOGOUT) {
                    evaluateView.showConfirmLogoutDialog();
                } else {
                    evaluateView.showMessage("Failed to sync data with server!");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                if (flag == FLAG_LOGOUT) {
                    evaluateView.showConfirmLogoutDialog();
                } else {
                    evaluateView.showMessage(t.getMessage());
                }
            }
        });
    }
}
