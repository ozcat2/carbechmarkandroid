package com.plaping.carbenchmark.app.settings.fragment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jakewharton.processphoenix.ProcessPhoenix;
import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.login.activity.LoginActivity;
import com.plaping.carbenchmark.base.BaseFragment;
import com.plaping.carbenchmark.utilties.AppSession;
import com.plaping.carbenchmark.utilties.L;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by SAINTZST on 11/16/2014.
 */
public class SettingFragment extends BaseFragment {

    public static final String TAG = SettingFragment.class.getSimpleName();

    @BindView(R.id.edt_endpoint)
    EditText edtEndpoint;

    public SettingFragment() {
        super();
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        ButterKnife.bind(this, rootView);
        edtEndpoint.setText(AppSession.getInstance().getApiEndpoint());
        edtEndpoint.setSelection(edtEndpoint.getText().length());
    }

    @OnClick(R.id.btn_save_endpoint)
    void onSaveClicked() {
        if (edtEndpoint.getText().toString().equals(AppSession.getInstance().getApiEndpoint())) {
            finishWithDefaultTransition();
        } else {
            AppSession.getInstance().setApiEndpoint(edtEndpoint.getText().toString());
            Intent intent = new Intent(context, LoginActivity.class);
            ProcessPhoenix.triggerRebirth(context, intent);
        }
    }

}
