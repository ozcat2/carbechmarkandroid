package com.plaping.carbenchmark.app.evaluate.activity;

import android.os.Bundle;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.evaluate.fragment.view.EvaluateFragment;
import com.plaping.carbenchmark.base.BaseActivity;

public class EvaluateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_container, EvaluateFragment.newInstance(), EvaluateFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        EvaluateFragment fragment = (EvaluateFragment) findFragment(EvaluateFragment.TAG);
        if (fragment != null) {
            fragment.validateStateBeforeBack();
        } else {
            super.playDefaultTransition();
        }
    }
}
