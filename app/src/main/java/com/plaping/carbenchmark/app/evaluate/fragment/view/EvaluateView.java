package com.plaping.carbenchmark.app.evaluate.fragment.view;

import com.plaping.carbenchmark.pojo.Criteria;

import java.util.List;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public interface EvaluateView {

    void setCriteriaList(List<Criteria> criteriaList);

    void showMessage(String message);

    void navigateToLogin();

    void showConfirmLogoutDialog();
}
