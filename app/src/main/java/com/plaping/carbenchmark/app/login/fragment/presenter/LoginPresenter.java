package com.plaping.carbenchmark.app.login.fragment.presenter;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public interface LoginPresenter {

    void getEvent();

    void validateAndSave(String eventCode, String evaluatorId);
}
