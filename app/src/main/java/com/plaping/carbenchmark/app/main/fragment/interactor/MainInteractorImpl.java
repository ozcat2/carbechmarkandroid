package com.plaping.carbenchmark.app.main.fragment.interactor;

import com.plaping.carbenchmark.manager.http.api.ApiManager;
import com.plaping.carbenchmark.manager.http.callback.CallbackListener;
import com.plaping.carbenchmark.pojo.Data;
import com.plaping.carbenchmark.utilties.AppSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tachakornueng on 27/8/2018 AD
 */
public class MainInteractorImpl implements MainInteractor {

    @Override
    public void getEventById(CallbackListener listener) {
        Map<String, String> map = new HashMap<>();
        map.put("event_id", String.valueOf(AppSession.getInstance().getEventId()));
        map.put("evaluator_code", AppSession.getInstance().getEvaluatorCode());
        ApiManager.getInstance().getRequest(map, listener);
    }

    @Override
    public void syncData(Data data, CallbackListener listener) {
        ApiManager.getInstance().putData(data, listener);
    }
}
