package com.plaping.carbenchmark.app.login.fragment.view;

import com.plaping.carbenchmark.pojo.Data;

import java.util.List;

/**
 * Created by tachakornueng on 11/8/2018 AD
 */
public interface LoginView {

    void setEventIdListAdapter(Data[] eventIds);

    void navigateToMainPage();

    void showError(String error);
}
