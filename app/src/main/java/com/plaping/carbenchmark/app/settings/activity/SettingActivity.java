package com.plaping.carbenchmark.app.settings.activity;

import android.os.Bundle;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.settings.fragment.view.SettingFragment;
import com.plaping.carbenchmark.base.BaseActivity;

public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_container, SettingFragment.newInstance(), SettingFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.playDefaultTransition();
    }
}
