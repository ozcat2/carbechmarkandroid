package com.plaping.carbenchmark.app.main.fragment.view;

import com.plaping.carbenchmark.pojo.Evaluation;

import java.util.List;

/**
 * Created by tachakornueng on 13/8/2018 AD
 */
public interface MainFragmentView {

    void setEvaluationList(List<Evaluation> evaluationList);

    void navigateToLogin();

    void showError(String error);

    void showNoConnection();

    void showConfirmLogoutDialog();
}
