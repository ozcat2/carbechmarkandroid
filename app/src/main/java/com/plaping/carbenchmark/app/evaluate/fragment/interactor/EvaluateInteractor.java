package com.plaping.carbenchmark.app.evaluate.fragment.interactor;

import com.plaping.carbenchmark.manager.http.callback.CallbackListener;
import com.plaping.carbenchmark.pojo.Data;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public interface EvaluateInteractor {

    void syncData(Data data, CallbackListener listener);
}
