package com.plaping.carbenchmark.app.login.fragment.interactor;

import com.plaping.carbenchmark.manager.http.callback.CallbackListener;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public interface LoginInteractor {

    void getEvent(CallbackListener listener);

    void getEventById(int eventId, String evaluatorCode, CallbackListener listener);
}
