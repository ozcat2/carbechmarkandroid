package com.plaping.carbenchmark.app.evaluate.fragment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.pojo.Car;
import com.plaping.carbenchmark.pojo.Criteria;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public class CriteriaListAdapter extends RecyclerView.Adapter<CriteriaListAdapter.CriteriaViewHolder> {

    private List<Criteria> criteriaList;
    private CriteriaListener listener;

    public CriteriaListAdapter(CriteriaListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CriteriaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CriteriaViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_criteria, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CriteriaViewHolder holder, int position) {
        Criteria criteria = criteriaList.get(position);
        Context context = holder.itemView.getContext();
        holder.tvCriteria.setText(criteria.getName());
        holder.layoutRoot.setBackgroundColor(getColor(context, criteria.isSelecting() ? R.color.light_gray : isComplete(criteria) ? R.color.white : R.color.yellow));
        holder.itemView.setOnClickListener(view -> setSelecting(position));
    }

    @Override
    public int getItemCount() {
        return criteriaList == null ? 0 : criteriaList.size();
    }

    public void setCriteriaList(List<Criteria> criteriaList) {
        this.criteriaList = criteriaList;
    }

    private void setSelecting(int position) {
        for (int i = 0; i < criteriaList.size(); i++) {
            criteriaList.get(i).setSelecting(i == position);
            if (i == position) {
                listener.onCriteriaSelected(criteriaList.get(i));
            }
        }
        notifyDataSetChanged();
    }

    private int getColor(Context context, int res) {
        return ContextCompat.getColor(context, res);
    }

    private boolean isComplete(Criteria criteria) {
        for (Car car : criteria.getCars()) {
            if (car.getBenchmark().getPoint() == null && !car.getBenchmark().isDisable()) {
                return false;
            }
        }
        return true;
    }

    class CriteriaViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_item_criteria)
        FrameLayout layoutRoot;
        @BindView(R.id.tv_list_item_criteria)
        TextView tvCriteria;

        CriteriaViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CriteriaListener {

        void onCriteriaSelected(Criteria criteria);
    }
}
