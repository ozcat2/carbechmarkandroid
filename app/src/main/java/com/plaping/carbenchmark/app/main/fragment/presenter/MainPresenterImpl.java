package com.plaping.carbenchmark.app.main.fragment.presenter;

import com.google.gson.Gson;
import com.plaping.carbenchmark.app.main.fragment.interactor.MainInteractor;
import com.plaping.carbenchmark.app.main.fragment.interactor.MainInteractorImpl;
import com.plaping.carbenchmark.app.main.fragment.view.MainFragmentView;
import com.plaping.carbenchmark.manager.http.callback.CallbackListenerAdapter;
import com.plaping.carbenchmark.pojo.Car;
import com.plaping.carbenchmark.pojo.Criteria;
import com.plaping.carbenchmark.pojo.Data;
import com.plaping.carbenchmark.pojo.Evaluation;
import com.plaping.carbenchmark.utilties.AppSession;
import com.plaping.carbenchmark.utilties.L;
import com.plaping.carbenchmark.utilties.NetworkUtils;

import static com.plaping.carbenchmark.app.main.fragment.view.MainFragment.FLAG_LOGOUT;

/**
 * Created by tachakornueng on 13/8/2018 AD
 */
public class MainPresenterImpl implements MainPresenter {

    private MainFragmentView fragmentView;
    private MainInteractor interactor;

    public MainPresenterImpl(MainFragmentView fragmentView) {
        this.fragmentView = fragmentView;
        interactor = new MainInteractorImpl();
    }

    @Override
    public void syncData(int flag) {
        Data data = AppSession.getInstance().getData();
        if (data == null) {
            L.i("FETCH NEW DATA FROM SERVER");
            getEvent();
        } else {
            calculateCompletion(data);
            fragmentView.setEvaluationList(data.getEvaluations());
            if (NetworkUtils.getInstance().isConnected()) {
                L.i("SYNC DATA TO SERVER");
                interactor.syncData(data, new CallbackListenerAdapter() {
                    @Override
                    public void onSuccess(String response) {
                        super.onSuccess(response);
                        if (flag == FLAG_LOGOUT) {
                            fragmentView.navigateToLogin();
                        } else {
                            getEvent();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        if (flag == FLAG_LOGOUT) {
                            fragmentView.showConfirmLogoutDialog();
                        } else {
                            calculateCompletion(data);
                            fragmentView.setEvaluationList(data.getEvaluations());
                            fragmentView.showError("Sync data to server failed!");
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        if (flag == FLAG_LOGOUT) {
                            fragmentView.showConfirmLogoutDialog();
                        } else {
                            calculateCompletion(data);
                            fragmentView.setEvaluationList(data.getEvaluations());
                            fragmentView.showError(t.getMessage());
                        }
                    }
                });
            } else {
                L.w("No connection");
                if (flag == FLAG_LOGOUT) {
                    fragmentView.showConfirmLogoutDialog();
                } else {
                    calculateCompletion(data);
                    fragmentView.setEvaluationList(data.getEvaluations());
                    fragmentView.showNoConnection();
                }
            }
        }
    }

    private void getEvent() {
        interactor.getEventById(new CallbackListenerAdapter() {
            @Override
            public void onSuccess(String response) {
                Data data = new Gson().fromJson(response, Data.class);
                AppSession.getInstance().setData(data);
                if (data.getEvaluator() != null) {
                    AppSession.getInstance().setUserColor(data.getEvaluator().getIconColor());
                }
                calculateCompletion(data);
                fragmentView.setEvaluationList(data.getEvaluations());
            }
        });
    }

    private void calculateCompletion(Data data) {
        for (Evaluation evaluation : data.getEvaluations()) {
            int total = 0;
            int rated = 0;
            for (Criteria criteria : evaluation.getCriteria()) {
                total += criteria.getCars().size();
                for (Car car : criteria.getCars()) {
                    if (car.getBenchmark().getPoint() != null || car.getBenchmark().isDisable()) {
                        rated++;
                    }
                }
            }
            float completion = rated * 100 / total;
            evaluation.setCompletion(completion);
        }
    }
}
