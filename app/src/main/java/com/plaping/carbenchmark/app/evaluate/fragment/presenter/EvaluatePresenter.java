package com.plaping.carbenchmark.app.evaluate.fragment.presenter;

import com.plaping.carbenchmark.pojo.Criteria;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public interface EvaluatePresenter {

    void getCriteriaList();

    void saveEvaluation(Criteria criteria);

    void syncData(int flag);
}
