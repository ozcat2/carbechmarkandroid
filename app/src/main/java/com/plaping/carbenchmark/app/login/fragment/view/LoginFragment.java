package com.plaping.carbenchmark.app.login.fragment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.login.fragment.presenter.LoginPresenter;
import com.plaping.carbenchmark.app.login.fragment.presenter.LoginPresenterImpl;
import com.plaping.carbenchmark.app.main.activity.view.MainActivity;
import com.plaping.carbenchmark.app.settings.activity.SettingActivity;
import com.plaping.carbenchmark.base.BaseFragment;
import com.plaping.carbenchmark.pojo.Data;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by SAINTZST on 11/16/2014.
 */
public class LoginFragment extends BaseFragment implements LoginView {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.spn_event_id)
    BetterSpinner spnEventId;
    @BindView(R.id.edt_evaluator_id)
    EditText edtEvaluatorId;

    private LoginPresenter presenter;

    public LoginFragment() {
        super();
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        ButterKnife.bind(this, rootView);
        presenter = new LoginPresenterImpl(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, new ArrayList<>());
        spnEventId.setAdapter(adapter);
        edtEvaluatorId.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_GO) {
                onLoginClicked();
                return true;
            }
            return false;
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.getEvent();
    }

    @OnClick(R.id.btn_login)
    void onLoginClicked() {
        showLoadingIntermediate();
        presenter.validateAndSave(spnEventId.getText().toString(), edtEvaluatorId.getText().toString());
    }

    @OnClick(R.id.imv_setting)
    void onSettingClicked() {
        startActivity(new Intent(context, SettingActivity.class));
        playDefaultTransition();
    }

    @Override
    public void setEventIdListAdapter(Data[] eventIds) {
        ArrayAdapter<Data> adapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, eventIds);
        spnEventId.setAdapter(adapter);
    }

    @Override
    public void navigateToMainPage() {
        dismissLoadingIntermediate();
        startActivity(new Intent(context, MainActivity.class));
        finish();
        playDefaultTransition();
    }

    @Override
    public void showError(String error) {
        dismissLoadingIntermediate();
        if (getView() != null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT).show();
        }
    }
}
