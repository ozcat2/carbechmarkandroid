package com.plaping.carbenchmark.app.main.fragment.adapter;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.pojo.Evaluation;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tachakornueng on 13/8/2018 AD
 */
public class EvaluationListAdapter extends RecyclerView.Adapter<EvaluationListAdapter.LevelViewHolder> {

    private List<Evaluation> evaluations;
    private EvaluationListener listener;

    public EvaluationListAdapter(EvaluationListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public LevelViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new LevelViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_evaluation, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LevelViewHolder holder, int position) {
        Evaluation evaluation = evaluations.get(position);
        holder.tvLevelNo.setText(evaluation.getLevel());
        if (position % 2 == 0) {
            holder.rootView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
        } else {
            holder.rootView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.extra_light_gray));
        }
        holder.tvName.setText(evaluation.getName());
        holder.btnEvaluate.setOnClickListener(view -> listener.onEvaluationClicked(evaluation));
        holder.tvStatus.setText(evaluation.getCompletion() < 100 ?
                String.format(new Locale("th"), "%d %%", (int) evaluation.getCompletion()) : "Completed");
    }

    @Override
    public int getItemCount() {
        return evaluations == null ? 0 : evaluations.size();
    }

    public void setEvaluations(List<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    class LevelViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_level_no)
        TextView tvLevelNo;
        @BindView(R.id.list_item_evaluation)
        FrameLayout rootView;
        @BindView(R.id.btn_evaluate)
        Button btnEvaluate;
        @BindView(R.id.tv_evaluation_name)
        TextView tvName;
        @BindView(R.id.tv_evaluation_status)
        TextView tvStatus;

        LevelViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EvaluationListener {

        void onEvaluationClicked(Evaluation evaluation);
    }
}
