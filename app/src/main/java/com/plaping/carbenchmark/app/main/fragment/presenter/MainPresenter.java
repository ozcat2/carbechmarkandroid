package com.plaping.carbenchmark.app.main.fragment.presenter;

/**
 * Created by tachakornueng on 13/8/2018 AD
 */
public interface MainPresenter {

    void syncData(int flag);
}
