package com.plaping.carbenchmark.app.login.activity;

import android.content.Intent;
import android.os.Bundle;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.login.fragment.view.LoginFragment;
import com.plaping.carbenchmark.app.main.activity.view.MainActivity;
import com.plaping.carbenchmark.base.BaseActivity;
import com.plaping.carbenchmark.utilties.AppSession;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        if (AppSession.getInstance().isLoggedIn()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_container, LoginFragment.newInstance(), LoginFragment.TAG)
                        .commit();
            }
        }
    }
}
