package com.plaping.carbenchmark.app.main.fragment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.evaluate.activity.EvaluateActivity;
import com.plaping.carbenchmark.app.login.activity.LoginActivity;
import com.plaping.carbenchmark.app.main.fragment.adapter.EvaluationListAdapter;
import com.plaping.carbenchmark.app.main.fragment.presenter.MainPresenter;
import com.plaping.carbenchmark.app.main.fragment.presenter.MainPresenterImpl;
import com.plaping.carbenchmark.base.BaseFragment;
import com.plaping.carbenchmark.components.ui.actionbar.MyActionBar;
import com.plaping.carbenchmark.components.ui.actionbar.MyActionBarListener;
import com.plaping.carbenchmark.pojo.Evaluation;
import com.plaping.carbenchmark.utilties.AppSession;
import com.plaping.carbenchmark.utilties.AppUtils;
import com.plaping.carbenchmark.utilties.L;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by SAINTZST on 11/16/2014.
 */
public class MainFragment extends BaseFragment implements MainFragmentView,
        EvaluationListAdapter.EvaluationListener, MyActionBarListener {

    public static final String TAG = MainFragment.class.getSimpleName();
    public static final int FLAG_SYNC = 0;
    public static final int FLAG_LOGOUT = 1;

    @BindView(R.id.rv_level_list)
    RecyclerView rvLevelList;
    @BindView(R.id.action_bar_main)
    MyActionBar actionBar;

    private MainPresenter presenter;
    private EvaluationListAdapter adapter;

    public MainFragment() {
        super();
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        ButterKnife.bind(this, rootView);
        actionBar.setListener(this);
        presenter = new MainPresenterImpl(this);
        adapter = new EvaluationListAdapter(this);
        rvLevelList.setLayoutManager(new LinearLayoutManager(context));
        rvLevelList.setAdapter(adapter);
        showLoadingIntermediate();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.syncData(FLAG_SYNC);
    }

    @Override
    public void onEvaluationClicked(Evaluation evaluation) {
        Intent intent = new Intent(context, EvaluateActivity.class);
        intent.putExtra("id", evaluation.getId());
        intent.putExtra("name", evaluation.getName());
        startActivity(intent);
        playDefaultTransition();
    }

    @Override
    public void setEvaluationList(List<Evaluation> evaluationList) {
        dismissLoadingIntermediate();
        adapter.setEvaluations(evaluationList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToLogin() {
        dismissLoadingIntermediate();
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppSession.getInstance().logout();
    }

    @Override
    public void showError(String error) {
        dismissLoadingIntermediate();
        if (getView() != null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showNoConnection() {
        dismissLoadingIntermediate();
        if (getView() != null) {
            Snackbar snackBar = Snackbar.make(getView(), "No internet connection", Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction("Dismiss", v -> snackBar.dismiss());
            snackBar.show();
        }
    }

    @Override
    public void showConfirmLogoutDialog() {
        dismissLoadingIntermediate();
        new MaterialDialog(context)
                .title(0, "Are you sure?")
                .message(0, "Sync failed! The data has not been saved")
                .positiveButton(0, "LOGOUT", materialDialog -> {
                    navigateToLogin();
                    return null;
                })
                .negativeButton(0, "CANCEL", null)
                .show();
    }

    @Override
    public void onLogout() {
        new MaterialDialog(context)
                .title(0, "Confirm")
                .message(0, "Are you sure you wish to logout?")
                .positiveButton(0, "LOGOUT", materialDialog -> {
                    showLoadingIntermediate();
                    presenter.syncData(FLAG_LOGOUT);
                    return null;
                })
                .negativeButton(0, "CANCEL", null)
                .show();
    }
}
