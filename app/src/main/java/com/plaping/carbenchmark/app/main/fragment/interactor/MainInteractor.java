package com.plaping.carbenchmark.app.main.fragment.interactor;

import com.plaping.carbenchmark.manager.http.callback.CallbackListener;
import com.plaping.carbenchmark.pojo.Data;

/**
 * Created by tachakornueng on 27/8/2018 AD
 */
public interface MainInteractor {

    void getEventById(CallbackListener listener);

    void syncData(Data data, CallbackListener listener);
}
