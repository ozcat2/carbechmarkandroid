package com.plaping.carbenchmark.app.login.fragment.presenter;

import com.google.gson.Gson;
import com.plaping.carbenchmark.app.login.fragment.interactor.LoginInteractor;
import com.plaping.carbenchmark.app.login.fragment.interactor.LoginInteractorImpl;
import com.plaping.carbenchmark.app.login.fragment.view.LoginView;
import com.plaping.carbenchmark.manager.http.callback.CallbackListenerAdapter;
import com.plaping.carbenchmark.pojo.Data;
import com.plaping.carbenchmark.pojo.Event;
import com.plaping.carbenchmark.utilties.AppSession;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;
    private LoginInteractor interactor;
    private AppSession session;
    private Event event;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        interactor = new LoginInteractorImpl();
        session = AppSession.getInstance();
    }

    @Override
    public void getEvent() {
        interactor.getEvent(new CallbackListenerAdapter() {
            @Override
            public void onSuccess(String response) {
                event = new Gson().fromJson(response, Event.class);
                Data[] datas = event.getData().toArray(new Data[event.getData().size()]);
                loginView.setEventIdListAdapter(datas);
            }
        });
    }

    @Override
    public void validateAndSave(String eventCode, String evaluatorId) {
        int eventId = 0;
        if (event != null && eventCode != null && !eventCode.equals("") && evaluatorId != null && !evaluatorId.equals("")) {
            for (Data data : event.getData()) {
                if (data.getCode().equals(eventCode)) {
                    eventId = data.getId();
                    break;
                }
            }
            interactor.getEventById(eventId, evaluatorId, new CallbackListenerAdapter() {
                @Override
                public void onSuccess(String response) {
                    Data data = new Gson().fromJson(response, Data.class);
                    session.setData(data);
                    session.setLoggedIn();
                    if (data.getEvaluator() != null) {
                        AppSession.getInstance().setUserColor(data.getEvaluator().getIconColor());
                    }
                    loginView.navigateToMainPage();
                }

                @Override
                public void onError(String error) {
                    loginView.showError(error);
                }

                @Override
                public void onFailure(Throwable t) {
                    loginView.showError(t.getMessage());
                }
            });
            session.setEventId(eventId);
            session.setEvaluatorCode(evaluatorId);
        } else if (eventCode == null || eventCode.equals("")) {
            loginView.showError("Please select event ID");
        } else if (evaluatorId == null || evaluatorId.equals("")) {
            loginView.showError("Please specify evaluator ID");
        }
    }
}
