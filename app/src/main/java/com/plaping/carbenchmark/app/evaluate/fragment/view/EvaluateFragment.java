package com.plaping.carbenchmark.app.evaluate.fragment.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.app.evaluate.fragment.adapter.CarModelListAdapter;
import com.plaping.carbenchmark.app.evaluate.fragment.adapter.CriteriaListAdapter;
import com.plaping.carbenchmark.app.evaluate.fragment.presenter.EvaluatePresenter;
import com.plaping.carbenchmark.app.evaluate.fragment.presenter.EvaluatePresenterImpl;
import com.plaping.carbenchmark.app.login.activity.LoginActivity;
import com.plaping.carbenchmark.base.BaseFragment;
import com.plaping.carbenchmark.components.ui.actionbar.MyActionBar;
import com.plaping.carbenchmark.components.ui.actionbar.MyActionBarListener;
import com.plaping.carbenchmark.pojo.Car;
import com.plaping.carbenchmark.pojo.Criteria;
import com.plaping.carbenchmark.utilties.AppSession;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by SAINTZST on 11/16/2014.
 */
public class EvaluateFragment extends BaseFragment implements EvaluateView,
        CriteriaListAdapter.CriteriaListener, CarModelListAdapter.CarModelListener,
        MyActionBarListener {

    public static final String TAG = EvaluateFragment.class.getSimpleName();
    private static final int STATE_EVALUATE = 0;
    private static final int STATE_COMMENT = 1;
    public static final int FLAG_SYNC = 0;
    public static final int FLAG_LOGOUT = 1;

    @BindView(R.id.action_bar_evaluate)
    MyActionBar actionBar;
    @BindView(R.id.rv_criteria)
    RecyclerView rvCriteria;
    @BindView(R.id.rv_car_model)
    RecyclerView rvCarModel;
    @BindView(R.id.tv_criteria_name)
    TextView tvName;
    @BindView(R.id.tv_criteria_description)
    TextView tvDescription;
    @BindView(R.id.tv_score_name_1)
    TextView tvScoreName1;
    @BindView(R.id.tv_score_name_2)
    TextView tvScoreName2;
    @BindView(R.id.tv_score_name_3)
    TextView tvScoreName3;
    @BindView(R.id.tv_score_name_4)
    TextView tvScoreName4;
    @BindView(R.id.tv_score_name_5)
    TextView tvScoreName5;

    @BindView(R.id.layout_car_model_list)
    LinearLayout layoutCarList;
    @BindView(R.id.layout_car_model_comment)
    LinearLayout layoutComment;

    @BindView(R.id.imv_car_model_comment_logo)
    ImageView imvCarLogo;
    @BindView(R.id.tv_car_model_comment_brand)
    TextView tvCarBrand;
    @BindView(R.id.edt_car_model_comment)
    EditText edtComment;
    @BindView(R.id.layout_divider)
    LinearLayout layoutDivider;

    private int state;
    private EvaluatePresenter presenter;
    private CriteriaListAdapter criteriaListAdapter;
    private CarModelListAdapter carModelListAdapter;

    public EvaluateFragment() {
        super();
    }

    public static EvaluateFragment newInstance() {
        EvaluateFragment fragment = new EvaluateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_evaluate, container, false);
        initInstances(rootView);
        adjustLayoutDividerPosition();
        return rootView;
    }

    private void initInstances(View rootView) {
        ButterKnife.bind(this, rootView);
        actionBar.setText(getBundle().getString("name", "")).setListener(this);
        presenter = new EvaluatePresenterImpl(this, getIntent().getIntExtra("id", -1));
        criteriaListAdapter = new CriteriaListAdapter(this);
        rvCriteria.setLayoutManager(new LinearLayoutManager(context));
        rvCriteria.setAdapter(criteriaListAdapter);
        carModelListAdapter = new CarModelListAdapter(this);
        rvCarModel.setLayoutManager(new LinearLayoutManager(context));
        rvCarModel.setAdapter(carModelListAdapter);
        rvCarModel.setNestedScrollingEnabled(false);
        presenter.getCriteriaList();
    }

    @Override
    public void onPause() {
        saveComment();
        presenter.saveEvaluation(carModelListAdapter.getCriteria());
        super.onPause();
    }

    private void adjustLayoutDividerPosition() {
        ViewTreeObserver vto = layoutDivider.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layoutDivider.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutDivider.getLayoutParams();
                int widthBefore = layoutDivider.getMeasuredWidth();
                int spaceBefore = widthBefore / 5 / 2;
                int leftMargin = params.getMarginStart();
                int rightMargin = params.getMarginEnd();
                int widthAfter = widthBefore + (spaceBefore * 2);
                int spaceAfter = widthAfter / 5 / 2;
                int gap = spaceAfter - spaceBefore;
                gap += gap / 2;
                params.setMarginStart(leftMargin - (spaceBefore + gap));
                params.setMarginEnd(rightMargin - (spaceBefore + gap));
                layoutDivider.setLayoutParams(params);
            }
        });
    }

    @Override
    public void setCriteriaList(List<Criteria> criteriaList) {
        criteriaListAdapter.setCriteriaList(criteriaList);
        criteriaListAdapter.notifyDataSetChanged();
        onCriteriaSelected(criteriaList.get(0));
    }

    @Override
    public void showMessage(String message) {
        dismissLoadingIntermediate();
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void navigateToLogin() {
        dismissLoadingIntermediate();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        AppSession.getInstance().logout();
    }

    @Override
    public void showConfirmLogoutDialog() {
        dismissLoadingIntermediate();
        new MaterialDialog(context)
                .title(0, "Are you sure?")
                .message(0, "Sync failed! The data has not been saved")
                .positiveButton(0, "LOGOUT", materialDialog -> {
                    navigateToLogin();
                    return null;
                })
                .negativeButton(0, "CANCEL", null)
                .show();
    }

    @Override
    public void onCriteriaSelected(Criteria criteria) {
        tvName.setText(String.format("Evaluation Criteria: %s", criteria.getName()));
        if (carModelListAdapter.getCriteria() != null) {
            saveComment();
            presenter.saveEvaluation(carModelListAdapter.getCriteria());
        }
        tvDescription.setText(criteria.getDescription());
        tvScoreName1.setText(criteria.getScores().getName1());
        tvScoreName2.setText(criteria.getScores().getName2());
        tvScoreName3.setText(criteria.getScores().getName3());
        tvScoreName4.setText(criteria.getScores().getName4());
        tvScoreName5.setText(criteria.getScores().getName5());
        carModelListAdapter.setCriteria(criteria);
        carModelListAdapter.notifyDataSetChanged();
        showCarListLayout();
    }

    @OnClick(R.id.btn_submit_form)
    void onSubmitClicked() {
        showLoadingIntermediate();
        saveComment();
        presenter.saveEvaluation(carModelListAdapter.getCriteria());
        presenter.syncData(FLAG_SYNC);
    }

    @OnClick(R.id.btn_save_comment)
    void onSaveCommentClicked() {
        showCarListLayout();
        saveComment();
    }

    @Override
    public void onCommentClicked(Car car) {
        showCommentLayout();
        Glide.with(context)
                .load(AppSession.getInstance().getApiEndpoint().concat(car.getLogoUrl()))
                .into(imvCarLogo);
        tvCarBrand.setText(String.format("%s\n%s", car.getBrand(), car.getModel()));
        if (car.getBenchmark().getComment() != null) {
            edtComment.setText(car.getBenchmark().getComment());
            edtComment.setSelection(edtComment.getText().length());
        } else {
            edtComment.getText().clear();
        }
    }

    @Override
    public void onLogout() {
        saveComment();
        presenter.saveEvaluation(carModelListAdapter.getCriteria());
        new MaterialDialog(context)
                .title(0, "Confirm")
                .message(0, "Are you sure you wish to logout?")
                .positiveButton(0, "LOGOUT", materialDialog -> {
                    showLoadingIntermediate();
                    presenter.syncData(FLAG_LOGOUT);
                    return null;
                })
                .negativeButton(0, "CANCEL", null)
                .show();
    }

    public void validateStateBeforeBack() {
        if (state == STATE_COMMENT) {
            showCarListLayout();
            saveComment();
        } else {
            finishWithDefaultTransition();
        }
    }

    private void showCarListLayout() {
        state = STATE_EVALUATE;
        hideSoftInputKeyboard();
        layoutCarList.setVisibility(View.VISIBLE);
        layoutComment.setVisibility(View.GONE);
    }

    private void showCommentLayout() {
        state = STATE_COMMENT;
        layoutCarList.setVisibility(View.GONE);
        layoutComment.setVisibility(View.VISIBLE);
    }

    private void saveComment() {
        if (!edtComment.getText().toString().equals("")) {
            carModelListAdapter.setCarComment(edtComment.getText().toString());
            edtComment.getText().clear();
        }
    }
}
