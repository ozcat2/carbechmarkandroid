package com.plaping.carbenchmark.app.evaluate.fragment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.pojo.Car;
import com.plaping.carbenchmark.pojo.Criteria;
import com.plaping.carbenchmark.pojo.Score;
import com.plaping.carbenchmark.utilties.AppSession;
import com.plaping.carbenchmark.utilties.NumberUtils;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by saintzst on 8/18/2018 AD
 */
public class CarModelListAdapter extends RecyclerView.Adapter<CarModelListAdapter.CarModelViewHolder> {

    private Criteria criteria;
    private CarModelListener listener;

    public CarModelListAdapter(CarModelListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CarModelViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CarModelViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_car_model, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarModelViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Context context = holder.itemView.getContext();
        Car car = criteria.getCars().get(position);
        holder.tvCarModelNo.setText(String.format(new Locale("th"), "%d", position + 1));
        holder.seekBar.setPadding(0, 0, 0, 0);
        float value1 = criteria.getScores().getValue1();
        float value2 = criteria.getScores().getValue2();
        float value3 = criteria.getScores().getValue3();
        float value4 = criteria.getScores().getValue4();
        float value5 = criteria.getScores().getValue5();
        holder.seekBar.setEnabled(!car.getBenchmark().isDisable());
        holder.seekBar.setOnTouchListener((view, motionEvent) -> car.getBenchmark().isDisable());
        holder.btnNa.setChecked(car.getBenchmark().isDisable());
        holder.seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                int progress = seekParams.progress;
                float score = 0;
                if (progress >= 0 && progress <= 25) {
                    score = getActualScore(progress, 0, 25, value1, value2);
                } else if (progress > 25 && progress <= 50) {
                    score = getActualScore(progress, 25, 50, value2, value3);
                } else if (progress > 50 && progress <= 75) {
                    score = getActualScore(progress, 50, 75, value3, value4);
                } else if (progress > 75 && progress <= 100) {
                    score = getActualScore(progress, 75, 100, value4, value5);
                }
                if (criteria.getScores().getSlideType().equals("int")) {
                    car.getBenchmark().setPoint((float) (int) score);
                } else if (criteria.getScores().getSlideType().equals("float")) {
                    car.getBenchmark().setPoint(NumberUtils.round(score, 1));
                }
                setValue(holder.tvValue, progress);
                updateCar(car);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        holder.tvBrand.setText(String.format("%s %s", car.getBrand(), car.getModel()));
        Glide.with(context)
                .load(AppSession.getInstance().getApiEndpoint().concat(car.getLogoUrl()))
                .apply(new RequestOptions().placeholder(R.drawable.icon_ve))
                .into(holder.imvCarModelLogo);
        if (car.getBenchmark().getPoint() != null) {
            float progress = 0;
            float point = car.getBenchmark().getPoint();
            if (point > value1 && point <= value2) {
                progress = getActualScore(point, value1, value2, 0.0f, 25.0f);
            } else if (point > value2 && point <= value3) {
                progress = getActualScore(point, value2, value3, 25.0f, 50.0f);
            } else if (point > value3 && point <= value4) {
                progress = getActualScore(point, value3, value4, 50.0f, 75.0f);
            } else if (point > value4 && point <= value5) {
                progress = getActualScore(point, value4, value5, 75.0f, 100.0f);
            }
            holder.seekBar.setProgress(criteria.getScores().getSlideType().equals("float") ? progress : progress);
            setValue(holder.tvValue, holder.seekBar.getProgress());
        } else {
            setValue(holder.tvValue, 0);
        }
        holder.imvComment.setOnClickListener(view -> {
            AppSession.getInstance().setCarId(car.getId());
            listener.onCommentClicked(car);
        });
        holder.layoutNa.setVisibility(criteria.getScores().isNaEnable() ? View.VISIBLE : View.GONE);
        if (criteria.getScores().isNaEnable()) {
            holder.layoutNa.setOnClickListener(view -> {
                holder.btnNa.setChecked(!holder.btnNa.isChecked());
                car.getBenchmark().setDisable(holder.btnNa.isChecked());
                notifyDataSetChanged();
            });
        }
    }

    private float getActualScore(float input, float min, float max, float targetLb, float targetUb) {
        float percent = (((input - min) * 100.0f) / (max - min)) / 100.0f;
        float value = (targetUb - targetLb) * percent;
        return targetLb + value;
    }

    @Override
    public int getItemCount() {
        return criteria == null || criteria.getCars() == null ? 0 : criteria.getCars().size();
    }

    private void setValue(TextView textView, int progress) {
        float value1 = criteria.getScores().getValue1();
        float value2 = criteria.getScores().getValue2();
        float value3 = criteria.getScores().getValue3();
        float value4 = criteria.getScores().getValue4();
        float value5 = criteria.getScores().getValue5();
        float score = 0;
        if (progress >= 0 && progress <= 25) {
            score = getActualScore(progress, 0, 25, value1, value2);
        } else if (progress > 25 && progress <= 50) {
            score = getActualScore(progress, 25, 50, value2, value3);
        } else if (progress > 50 && progress <= 75) {
            score = getActualScore(progress, 50, 75, value3, value4);
        } else if (progress > 75 && progress <= 100) {
            score = getActualScore(progress, 75, 100, value4, value5);
        }
        if (criteria.getScores().getSlideType().equals("int")) {
            textView.setText(String.format(new Locale("th"), "%03d", (int) score));
        } else if (criteria.getScores().getSlideType().equals("float")) {
            textView.setText(String.format(new Locale("th"), "%.1f", score));
        }
    }

    private void updateCar(Car mCar) {
        int carId = mCar.getId();
        if (criteria != null && criteria.getCars() != null) {
            for (int i = 0; i < criteria.getCars().size(); i++) {
                Car car = criteria.getCars().get(i);
                if (car.getId() == carId) {
                    criteria.getCars().set(i, mCar);
                    break;
                }
            }
        }
    }

    private int getMinimumScore() {
        int min = 0;
        if (criteria != null) {
            Score score = criteria.getScores();
            if (score != null) {
                min = (int) score.getValue1();
                if (score.getValue2() < min) {
                    min = (int) score.getValue2();
                }
                if (score.getValue3() < min) {
                    min = (int) score.getValue3();
                }
                if (score.getValue4() < min) {
                    min = (int) score.getValue4();
                }
                if (score.getValue5() < min) {
                    min = (int) score.getValue5();
                }
            }
        }
        return min;
    }

    private int getMaximumScore() {
        int max = 100;
        if (criteria != null) {
            Score score = criteria.getScores();
            if (score != null) {
                max = (int) score.getValue1();
                if (score.getValue2() > max) {
                    max = (int) score.getValue2();
                }
                if (score.getValue3() > max) {
                    max = (int) score.getValue3();
                }
                if (score.getValue4() > max) {
                    max = (int) score.getValue4();
                }
                if (score.getValue5() > max) {
                    max = (int) score.getValue5();
                }
            }
        }
        return max;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCarComment(String comment) {
        int carId = AppSession.getInstance().getCarId();
        if (criteria != null && criteria.getCars() != null) {
            for (int i = 0; i < criteria.getCars().size(); i++) {
                if (criteria.getCars().get(i).getId() == carId) {
                    criteria.getCars().get(i).getBenchmark().setComment(comment);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    class CarModelViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imv_car_model_logo)
        ImageView imvCarModelLogo;
        @BindView(R.id.tv_car_brand)
        TextView tvBrand;
        @BindView(R.id.seek_bar)
        IndicatorSeekBar seekBar;
        @BindView(R.id.tv_car_benchmark_value)
        TextView tvValue;
        @BindView(R.id.imv_car_benchmark_comment)
        ImageView imvComment;
        @BindView(R.id.tv_car_model_no)
        TextView tvCarModelNo;
        @BindView(R.id.layout_btn_na)
        FrameLayout layoutNa;
        @BindView(R.id.btn_na)
        RadioButton btnNa;


        CarModelViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CarModelListener {

        void onCommentClicked(Car car);
    }
}
