package com.plaping.carbenchmark.base;

import android.annotation.SuppressLint;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.plaping.carbenchmark.R;

/**
 * Created by tachakornueng on 11/8/2018 AD
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    public void playTransition(int enterAnim, int exitAnim) {
        super.onBackPressed();
        overridePendingTransition(enterAnim, exitAnim);
    }

    public void playDefaultTransition() {
        super.onBackPressed();
        overridePendingTransition(R.anim.from_left_in, R.anim.to_right_out);
    }

    public BaseFragment findFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        return (BaseFragment) fragmentManager.findFragmentByTag(tag);
    }

}
