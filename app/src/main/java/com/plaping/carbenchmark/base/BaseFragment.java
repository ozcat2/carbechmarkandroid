package com.plaping.carbenchmark.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.plaping.carbenchmark.R;
import com.plaping.carbenchmark.components.dialog.LoadingDialogIntermediate;

import java.util.Objects;

/**
 * Created by tachakornueng on 11/8/2018 AD
 */
public class BaseFragment extends Fragment {

    public Context context;
    private LoadingDialogIntermediate loadingDialogIntermediate;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void playTransition(int enterAnim, int exitAnim) {
        if (getActivity() != null)
            getActivity().overridePendingTransition(enterAnim, exitAnim);
    }

    public void playDefaultTransition() {
        if (getActivity() != null) {
            getActivity().overridePendingTransition(R.anim.from_right_in, R.anim.to_left_out);
        }
    }

    public void setResult(int resultCode, Intent intent) {
        if (getActivity() != null)
            getActivity().setResult(resultCode, intent);
    }

    public void finish() {
        if (getActivity() != null)
            getActivity().finish();
    }

    public void finishWithDefaultTransition() {
        if (getActivity() != null) {
            getActivity().finish();
            playTransition(R.anim.from_left_in, R.anim.to_right_out);
        }
    }

    public void hideSoftInputKeyboard() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    public void showSoftInputKeyboard() {
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }
    }

    public void showLoadingIntermediate() {
        if (getFragmentManager() != null) {
            loadingDialogIntermediate = LoadingDialogIntermediate.newInstance();
            loadingDialogIntermediate.show(getFragmentManager(), LoadingDialogIntermediate.TAG);
        }
    }

    public void dismissLoadingIntermediate() {
        try {
            if (loadingDialogIntermediate != null) {
                loadingDialogIntermediate.dismiss();
            }
        } catch (NullPointerException | IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public Intent getIntent() {
        return Objects.requireNonNull(getActivity()).getIntent();
    }

    public Bundle getBundle() {
        if (getActivity() != null && getActivity().getIntent().getExtras() != null) {
            return getActivity().getIntent().getExtras();
        }
        return new Bundle();
    }

}
