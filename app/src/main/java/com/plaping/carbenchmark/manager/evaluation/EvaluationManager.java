package com.plaping.carbenchmark.manager.evaluation;

import android.content.Context;

import com.plaping.carbenchmark.base.Contextor;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class EvaluationManager {

    private static EvaluationManager instance;

    public static EvaluationManager getInstance() {
        if (instance == null)
            instance = new EvaluationManager();
        return instance;
    }

    private Context mContext;

    private EvaluationManager() {
        mContext = Contextor.getInstance().getContext();
    }

}
