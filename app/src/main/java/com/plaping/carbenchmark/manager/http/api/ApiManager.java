package com.plaping.carbenchmark.manager.http.api;

import android.content.Context;

import com.plaping.carbenchmark.base.Contextor;
import com.plaping.carbenchmark.manager.http.HttpManager;
import com.plaping.carbenchmark.manager.http.callback.CallbackListener;
import com.plaping.carbenchmark.manager.http.callback.CustomCallback;
import com.plaping.carbenchmark.pojo.Data;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class ApiManager {

    private static ApiManager instance;

    public static ApiManager getInstance() {
        if (instance == null)
            instance = new ApiManager();
        return instance;
    }

    private Context mContext;

    private ApiManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public void getRequest(CallbackListener listener) {
        Call<ResponseBody> call = HttpManager.getInstance().getService().getEvent();
        call.enqueue(new CustomCallback(listener));
    }

    public void getRequest(Map<String, String> map, CallbackListener listener) {
        Call<ResponseBody> call = HttpManager.getInstance().getService().getEventDetail(map);
        call.enqueue(new CustomCallback(listener));
    }

    public void putData(Data data, CallbackListener listener) {
        Call<ResponseBody> call = HttpManager.getInstance().getService().putData(data);
        call.enqueue(new CustomCallback(listener));
    }

}
