package com.plaping.carbenchmark.manager.http.callback;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public interface CallbackListener {

    void onSuccess(String response);

    void onError(String error);

    void onFailure(Throwable t);
}
