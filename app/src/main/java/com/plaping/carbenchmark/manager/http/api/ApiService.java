package com.plaping.carbenchmark.manager.http.api;

import com.plaping.carbenchmark.pojo.Data;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public interface ApiService {

    @Headers("Cache-Control: no-cache")
    @GET("api/mobile/event")
    Call<ResponseBody> getEvent();

    @Headers("Cache-Control: no-cache")
    @GET("api/mobile/event")
    Call<ResponseBody> getEventDetail(@QueryMap Map<String, String> map);

    @Headers("Cache-Control: no-cache")
    @PUT("api/mobile/event")
    Call<ResponseBody> putData(@Body Data data);

}
