package com.plaping.carbenchmark.manager.http.callback;

import android.support.annotation.NonNull;

import com.plaping.carbenchmark.utilties.L;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tachakornueng on 27/8/2018 AD
 */
public class CustomCallback implements Callback<ResponseBody> {

    private CallbackListener listener;

    public CustomCallback(CallbackListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        try {
            if (response.isSuccessful()) {
                String raw = Objects.requireNonNull(response.body()).string();
                listener.onSuccess(raw);
            } else {
                String error = Objects.requireNonNull(response.errorBody()).string();
                listener.onError(error);
            }
        } catch (IOException e) {
            listener.onError(e.getMessage());
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
        listener.onFailure(t);
    }
}
