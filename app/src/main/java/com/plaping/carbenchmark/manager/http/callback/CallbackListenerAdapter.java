package com.plaping.carbenchmark.manager.http.callback;

import com.plaping.carbenchmark.utilties.L;

/**
 * Created by saintzst on 8/23/2018 AD
 */
public class CallbackListenerAdapter implements CallbackListener {

    @Override
    public void onSuccess(String response) {
        L.d(response);
    }

    @Override
    public void onError(String error) {
        L.e(error);
    }

    @Override
    public void onFailure(Throwable t) {
        L.w(t.getMessage());
    }
}
