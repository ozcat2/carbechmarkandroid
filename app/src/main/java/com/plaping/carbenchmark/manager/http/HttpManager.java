package com.plaping.carbenchmark.manager.http;

import android.content.Context;

import com.plaping.carbenchmark.base.Contextor;
import com.plaping.carbenchmark.manager.http.api.ApiService;
import com.plaping.carbenchmark.utilties.AppSession;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SAINTZST on 11/16/2014.
 */
public class HttpManager {

    private static HttpManager instance;

    private ApiService service;

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private Context mContext;

    private HttpManager() {
        mContext = Contextor.getInstance().getContext();

        String baseUrl = AppSession.getInstance().getApiEndpoint();
        if (!baseUrl.endsWith("/")) {
            baseUrl = baseUrl.concat("/");
        }
        baseUrl = baseUrl.replace(" ", "");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(ApiService.class);
    }

    public ApiService getService() {
        return service;
    }
}
